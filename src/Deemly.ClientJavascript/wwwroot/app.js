﻿/// <reference path="oidc-client.js" />

function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerHTML += msg + '\r\n';
    });
}

document.getElementById("login").addEventListener("click", login, false);
//document.getElementById("api").addEventListener("click", api, false);
document.getElementById("logout").addEventListener("click", logout, false);

var config = {
    authority: "https://auth.deemly.co",
    client_id: "61e1962264f840459fd8943f02f28b25",
    redirect_uri: "http://deemly-client-js.azurewebsites.net/callback.html",
    response_type: "id_token token",
    scope:"openid profile",
    post_logout_redirect_uri: "http://deemly-client-js.azurewebsites.net/index.html"
};
var mgr = new Oidc.UserManager(config);

mgr.getUser().then(function (user) {
    var logout = document.getElementById('logout');
    if (user) {
        log("User logged in", user.profile);
        logout.style.visibility = '';
    }
    else {
        log("User not logged in");
        logout.style.visibility = 'hidden';
    }
});

function login() {
    mgr.signinRedirect();
}

//function api() {
//    mgr.getUser().then(function (user) {
//        var url = "http://localhost:5001/identity";

//        var xhr = new XMLHttpRequest();
//        xhr.open("GET", url);
//        xhr.onload = function () {
//            log(xhr.status, JSON.parse(xhr.responseText));
//        }
//        xhr.setRequestHeader("Authorization", "Bearer " + user.access_token);
//        xhr.send();
//    });
//}

function logout() {
    mgr.signoutRedirect();
}