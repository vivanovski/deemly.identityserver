﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;

namespace Deemly.AspNetCore.Authentication
{
    public static class OpenIdConnectExtensions
    {
        private const string DefaultAuthenticationScheme = "Deemly";
        private const string DefaultCallbackPath = "/signin-deemly";

        //
        // Summary:
        //     Adds the DeemlyLoginMiddleware middleware to the specified Microsoft.AspNetCore.Builder.IApplicationBuilder,
        //     which enables Deemly authentication capabilities.
        //
        // Parameters:
        //   app:
        //     The Microsoft.AspNetCore.Builder.IApplicationBuilder to add the middleware to.
        //
        //   options:
        //     A Microsoft.AspNetCore.Builder.OpenIdConnectOptions that specifies options for
        //     the middleware.
        //
        // Returns:
        //     A reference to this instance after the operation has completed.
        public static IApplicationBuilder UseDeemlyAuthentication(this IApplicationBuilder app, DeemlyOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            options.SignInScheme = "Cookies";
            //options.Authority = "https://auth-deemly-dev.azurewebsites.net";
            options.Authority = "http://localhost:5000";
            options.RequireHttpsMetadata = false;
            options.GetClaimsFromUserInfoEndpoint = true;

            if (options.AuthenticationScheme == "OpenIdConnect")
            {
                options.AuthenticationScheme = DefaultAuthenticationScheme;
                options.DisplayName = DefaultAuthenticationScheme;
            }

            if (options.CallbackPath.Value == "/signin-oidc")
            {
                options.CallbackPath = new PathString(DefaultCallbackPath);
            }

            options.TokenValidationParameters = new TokenValidationParameters() { NameClaimType = "name" };

            options.Events = new OpenIdConnectEvents
            {
                OnRemoteFailure = context =>
                {
                    //this section added to handle scenario where user logs in, but cancels consenting to rights to read directory profile
                    string appBaseUrl = context.Request.Scheme + "://" + context.Request.Host + context.Request.PathBase + "/";

                    context.HandleResponse();
                    context.Response.Redirect(appBaseUrl);

                    return Task.FromResult(0);
                },
                OnAuthenticationFailed = context =>
                {
                    return Task.FromResult(0);
                }
            };

            return app.UseOpenIdConnectAuthentication(options);
        }
    }

    public class DeemlyOptions : OpenIdConnectOptions
    {
        public DeemlyOptions() : base() { }
        public DeemlyOptions(string authenticationScheme) : base(authenticationScheme) { }
    }
}
