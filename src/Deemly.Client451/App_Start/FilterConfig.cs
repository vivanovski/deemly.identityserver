﻿using System.Web;
using System.Web.Mvc;

namespace Deemly.Client451
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
