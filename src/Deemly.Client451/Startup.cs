﻿using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Deemly.Authentication;

[assembly: OwinStartupAttribute(typeof(Deemly.Client451.Startup))]
namespace Deemly.Client451
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions { });

            app.UseDeemlyAuthentication(
                new DeemlyOptions
                {
                    ClientId = "mvc",
                    Authority = "http://localhost:5000",
                    ClientSecret = "secret",
                    Scope = "api1",
                    ResponseType = "code id_token"
                });
        }
    }
}
