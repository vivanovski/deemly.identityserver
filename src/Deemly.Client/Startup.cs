﻿using Deemly.AspNetCore.Authentication;
using Deemly.Client.Data;
using Deemly.Client.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Deemly.Client
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        private IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            string connectionString = Configuration["Data:DefaultConnection:ConnectionString"];

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));
            services.AddIdentity<ApplicationUser, IdentityRole>(options => {
                options.Cookies.ExternalCookie.AuthenticationScheme = "Deemly";
            }).AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {            
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseIdentity();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies"
            });

            //JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            app.UseDeemlyAuthentication(new DeemlyOptions
            {
                ClientId = "b973ca7ab9ac4f0390a07aa11992815a",
                ClientSecret = "WM3RduU1v5y/TMP19eR4ApIwIMt/19lX0rCZaaiK6Jw=",
                ResponseType = "code id_token",
                SaveTokens = true
            });
            
            app.UseStaticFiles();                        
            app.UseMvcWithDefaultRoute();
        }
    }
}
