﻿using System;
using System.IdentityModel.Tokens;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;

namespace Deemly.AspNet.Authentication
{
    public static class OpenIdConnectExtensions
    {
        private const string DefaultAuthenticationScheme = "Deemly";
        private const string DefaultCallbackPath = "/signin-deemly";

        //
        // Summary:
        //     Adds the DeemlyLoginMiddleware middleware to the specified Microsoft.AspNetCore.Builder.IApplicationBuilder,
        //     which enables Deemly authentication capabilities.
        //
        // Parameters:
        //   app:
        //     The Microsoft.AspNetCore.Builder.IApplicationBuilder to add the middleware to.
        //
        //   options:
        //     A Microsoft.AspNetCore.Builder.OpenIdConnectOptions that specifies options for
        //     the middleware.
        //
        // Returns:
        //     A reference to this instance after the operation has completed.
        public static IAppBuilder UseDeemlyAuthentication(this IAppBuilder app, DeemlyOptions options)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            options.SignInAsAuthenticationType = "Cookies";
            options.Authority = "https://auth.deemly.co";
            //options.RequireHttpsMetadata = false;
            //options.GetClaimsFromUserInfoEndpoint = true;

            if (options.AuthenticationType == "OpenIdConnect")
            {
                options.AuthenticationType = DefaultAuthenticationScheme;
                options.Caption = DefaultAuthenticationScheme;
            }

            if (options.CallbackPath.Value == "/signin-oidc")
            {
                options.CallbackPath = new PathString(DefaultCallbackPath);
            }

            options.TokenValidationParameters = new TokenValidationParameters() { NameClaimType = "name" };

            options.Notifications = new OpenIdConnectAuthenticationNotifications
            {
                AuthenticationFailed = context =>
                {
                    //this section added to handle scenario where user logs in, but cancels consenting to rights to read directory profile
                    string appBaseUrl = context.Request.Scheme + "://" + context.Request.Host + context.Request.PathBase + "/";

                    context.HandleResponse();
                    context.Response.Redirect(appBaseUrl);

                    return Task.FromResult(0);
                }
            };            

            return app.UseOpenIdConnectAuthentication(options);
        }
    }
    public class DeemlyOptions : OpenIdConnectAuthenticationOptions
    {
        public DeemlyOptions() : base() { }

        public DeemlyOptions(string authenticationType) : base(authenticationType) { }
    }
}
