﻿using System;
using System.Collections.Generic;
using Deemly.AuthIdentityServer.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Deemly.AuthIdentityServer.Data;
using Deemly.AuthIdentityServer.Models;
using System.Reflection;
using IdentityServer4.EntityFramework.DbContexts;
using System.Linq;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Serilog.Events;
using IdentityServer4;

namespace Deemly.AuthIdentityServer
{
    public class Startup
    {
        private readonly bool _isDevelopment;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            _isDevelopment = env.IsDevelopment();
            builder.AddEnvironmentVariables();

            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Error).WriteTo.File($"../logs/ErrorLogs/{DateTime.Now:dd-MM-yyyy}.txt"))
            .WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Debug).WriteTo.File($"../logs/DebugLogs/{DateTime.Now:dd-MM-yyyy}.txt"))
            .WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Fatal).WriteTo.File($"../logs/FatalLogs/{DateTime.Now:dd-MM-yyyy}.txt"))
            .WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Verbose).WriteTo.File($"../logs/VerboseLogs/{DateTime.Now:dd-MM-yyyy}.txt"))
            .WriteTo.Logger(lc => lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Information).WriteTo.File($"../logs/InfoLogs/{DateTime.Now:dd-MM-yyyy}.txt"))
            .CreateLogger();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (!_isDevelopment)
            {
                services.Configure<MvcOptions>(options =>
                {
                    options.Filters.Add(new RequireHttpsAttribute());
                });
            }

            services.AddMvc();

            string connectionString = Configuration["Data:DefaultConnection:ConnectionString"];
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));

            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>();

            // configure identity server with in-memory users, but EF stores for clients and scopes
            var idSrv = services.AddIdentityServer()        
                .AddAspNetIdentity<ApplicationUser>()
                .AddConfigurationStore(builder => builder.UseSqlServer(connectionString, options => options.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(builder => builder.UseSqlServer(connectionString, options => options.MigrationsAssembly(migrationsAssembly)))
                .SetTemporarySigningCredential();

            idSrv.AddSecretValidator<PlainTextSharedSecretValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // this will do the initial DB population
            InitializeDatabase(app);

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug(LogLevel.Debug).AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }           

            app.UseIdentity();            
            app.UseIdentityServer();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,

                AutomaticAuthenticate = false,
                AutomaticChallenge = false
            });

            /* Configure Facebook authentication */
            app.UseFacebookAuthentication(new FacebookOptions
            {
                AppId = Configuration["ExternalIdentityProviders:Facebook:AppId"],
                AppSecret = Configuration["ExternalIdentityProviders:Facebook:AppSecret"],
                SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,
                SaveTokens = true
            });

            /* Configure Google authentication */
            app.UseGoogleAuthentication(new GoogleOptions
            {                
                ClientId = Configuration["ExternalIdentityProviders:Google:ClientId"],
                ClientSecret = Configuration["ExternalIdentityProviders:Google:ClientSecret"],
                SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme,
                SaveTokens = true
            });

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {                
                var context = scope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();             

                if (!context.Scopes.Any())
                {
                    foreach (var client in Config.GetScopes())
                    {
                        context.Scopes.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}
