﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Logging;
using Deemly.AuthIdentityServer.Models;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Http.Authentication;
using System;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using IdentityModel;
using IdentityModel.Client;
using IdentityServer4;

namespace Deemly.AuthIdentityServer.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        //private readonly ILogger _logger;

        public AccountController(
            UserManager<ApplicationUser> userManager,         
            //ILoggerFactory loggerFactory, 
            IIdentityServerInteractionService interaction, 
            IClientStore clientStore)
        {
            _userManager = userManager;
            _interaction = interaction;
            _clientStore = clientStore;
            //_logger = loggerFactory.CreateLogger<AccountController>();
        }
        
        //
        // GET: /Account/Login
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);            
            var vm = await BuildLoginViewModelAsync(returnUrl, context);            

            return View(vm);
        }

        public async Task<IActionResult> GetClientRedirectUrl(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            if (context != null)
                return Redirect(context.Parameters.Get("redirect_uri"));

            return Redirect("~/");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginInputModel model)
        {
            if (ModelState.IsValid)
            {
                var identityUser = await _userManager.FindByNameAsync(model.Email);
                if (identityUser != null && await _userManager.CheckPasswordAsync(identityUser, model.Password))
                {
                    AuthenticationProperties props = null;
                    // only set explicit expiration here if persistent. 
                    // otherwise we reply upon expiration configured in cookie middleware.
                    if (model.RememberLogin)
                    {
                        props = new AuthenticationProperties
                        {                           
                            IsPersistent = true,
                            ExpiresUtc = DateTimeOffset.UtcNow.AddMonths(1)
                        };
                    }

                    var additionalClaims = new []
                    {
                        new Claim(ClaimTypes.Name, identityUser.Name),   
                        new Claim("picture", string.IsNullOrEmpty(identityUser.ProfilePictureURL) ? string.Empty : identityUser.ProfilePictureURL)
                    };
                                        
                    await HttpContext.Authentication.SignInAsync(identityUser.Id, identityUser.UserName, "Deemly", props, additionalClaims);
                    if (_interaction.IsValidReturnUrl(model.ReturnUrl))
                    {                        
                        return Redirect(model.ReturnUrl);
                    }

                    return Redirect("~/");
                }
                ModelState.AddModelError("", "Invalid username or password.");
            }

            // something went wrong, show form with error
            var vm = await BuildLoginViewModelAsync(model);
            return View(vm);
        }

        /// <summary>
        /// initiate roundtrip to external authentication provider
        /// </summary>
        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            if (returnUrl != null)
            {
                returnUrl = UrlEncoder.Default.Encode(returnUrl);
            }
            returnUrl = "/account/externallogincallback?returnUrl=" + returnUrl;

            // start challenge and roundtrip the return URL
            return new ChallengeResult(provider, new AuthenticationProperties
            {
                RedirectUri = returnUrl
            });
        }

        /// <summary>
        /// Post processing of external authentication
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl)
        {
            // read external identity from the temporary cookie
            var tempUser = await HttpContext.Authentication.AuthenticateAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);
            if (tempUser == null)
            {
                throw new Exception("External authentication error");
            }

            // retrieve claims of the external user
            var claims = tempUser.Claims.ToList();

            // try to determine the unique id of the external user - the most common claim type for that are the sub claim and the NameIdentifier
            // depending on the external provider, some other claim type might be used
            var userIdClaim = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject);
            if (userIdClaim == null)
            {
                userIdClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
            }
            if (userIdClaim == null)
            {
                throw new Exception("Unknown userid");
            }

            // remove the user id claim from the claims collection and move to the userId property
            // also set the name of the external authentication provider
            claims.Remove(userIdClaim);
            var provider = userIdClaim.Issuer;
            var userId = userIdClaim.Value;

            var additionalClaims = new List<Claim>();
            var user = await _userManager.FindByLoginAsync(provider, userId);
            if (user == null || user.UserType != 1)
            {                                
                await HttpContext.Authentication.SignOutAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);

                string errorId = string.Format("Deemly user not registered with {0}.", provider);
                return RedirectToAction("Error", "Home", new { errorId, returnUrl });
            }

            additionalClaims.AddRange(new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Name),
                new Claim("picture", string.IsNullOrEmpty(user.ProfilePictureURL) ? string.Empty : user.ProfilePictureURL)
            });

            // if the external system sent a session id claim, copy it over
            var sid = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
            if (sid != null)
            {
                additionalClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));
            }            

            // issue authentication cookie for user
            await HttpContext.Authentication.SignInAsync(user.Id, user.UserName, provider, additionalClaims.ToArray());

            // delete temporary cookie used during external authentication
            await HttpContext.Authentication.SignOutAsync(IdentityServerConstants.ExternalCookieAuthenticationScheme);

            // validate return URL and redirect back to authorization endpoint                        
            if (_interaction.IsValidReturnUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return Redirect("~/");
        }        

        /// <summary>
        /// Show logout page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            // delete authentication cookie
            await HttpContext.Authentication.SignOutAsync();

            // set this so UI rendering sees an anonymous user
            HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity());

            // get context information (client name, post logout redirect URI and iframe for federated signout)
            var logoutContext = await _interaction.GetLogoutContextAsync(logoutId);
            if (logoutContext?.IsAuthenticatedLogout == true)
            {
                // if the logout request is authenticated, it's safe to automatically sign-out
                return Redirect(logoutContext.PostLogoutRedirectUri);
            }

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #region Helpers

        async Task<LoginViewModel> BuildLoginViewModelAsync(string returnUrl, AuthorizationRequest context)
        {
            var providers = HttpContext.Authentication.GetAuthenticationSchemes()
                .Where(x => x.DisplayName != null)
                .Select(x => new ExternalProvider
                {
                    DisplayName = x.DisplayName,
                    AuthenticationScheme = x.AuthenticationScheme
                });

            var allowLocal = true;
            if (context?.ClientId != null)
            {
                var client = await _clientStore.FindEnabledClientByIdAsync(context.ClientId);
                if (client != null)
                {
                    allowLocal = client.EnableLocalLogin;

                    if (client.IdentityProviderRestrictions != null && client.IdentityProviderRestrictions.Any())
                    {
                        providers = providers.Where(provider => client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme));
                    }
                }
            }

            return new LoginViewModel
            {
                EnableLocalLogin = allowLocal,
                ReturnUrl = returnUrl,
                Email = context?.LoginHint,
                ExternalProviders = providers.ToArray()
            };
        }

        async Task<LoginViewModel> BuildLoginViewModelAsync(LoginInputModel model)
        {
            var context = await _interaction.GetAuthorizationContextAsync(model.ReturnUrl);
            var vm = await BuildLoginViewModelAsync(model.ReturnUrl, context);
            vm.Email = model.Email;
            vm.RememberLogin = model.RememberLogin;
            return vm;
        }

        #endregion
    }
}
