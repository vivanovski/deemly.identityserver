﻿namespace Deemly.AuthIdentityServer.Models
{
    public class LogoutViewModel
    {
        public string LogoutId { get; set; }
    }
}
