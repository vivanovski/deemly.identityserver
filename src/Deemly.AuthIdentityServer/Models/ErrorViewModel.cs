﻿using IdentityServer4.Models;

namespace Deemly.AuthIdentityServer.Models
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}
