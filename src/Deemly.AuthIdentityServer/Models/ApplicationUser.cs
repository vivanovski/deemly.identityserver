﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Deemly.AuthIdentityServer.Models
{
    public class ApplicationUser : IdentityUser
    {
        public DateTime CreatedOn { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string About { get; set; }
        public virtual string ProfilePictureURL { get; set; }
        public virtual string ProfilePictureThumbnailURL { get; set; }
        public virtual string CoverPhotoURL { get; set; }
        public bool HasLoggedIn { get; set; }
        public bool IsApiServiceCustomer { get; set; }
        public int UserType { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileUserName { get; set; }        
    }
}
